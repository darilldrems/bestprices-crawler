first install the following dependencies for dryscrape to work
# apt-get install qt5-default libqt5webkit5-dev build-essential python-lxml python-pip xvfb

The not_product_pages.db file is invisible from sublime text or text editor. use ls -a to view it. And its purpose is to learn which pages should be filtered from links because they are not product pages