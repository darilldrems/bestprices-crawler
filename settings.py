#settings

settings = {
	"endpoints": {
		"Nigeria": {
			"get_stores_url": "http://bestprices.ng/backend/backoffice/api/stores?total=20&pass=golang-crawler",
			"get_products_url": "http://bestprices.ng/backend/backoffice/api/products/todo?pass=golang-crawler",
			"add_offer_url": "http://bestprices.ng/backend/backoffice/api/offer/new",
			"get_old_products_url": "http://bestprices.ng/backend/backoffice/api/pricechecker/products",
			"update_product_offers_url": ""
		}
	}
}

forward_slash_exceptions = ["payporte", "blessingcomputers", "yudala"]

hdr = {
		'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
		'Accept-Encoding': 'none',
		'Accept-Language': 'en-US,en;q=0.8',
		'Connection': 'keep-alive',
		}