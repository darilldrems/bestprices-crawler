#utilities
import re
import dryscrape
# import urllib2
import urllib.request
import urllib

from unicode_rep_currency import currencies
from forbiden import forbiden_words
from urllib.parse import urljoin
from bs4 import BeautifulSoup
from settings import forward_slash_exceptions, hdr
from learner import LinksLearner


def sanitize_data(data, typ=""):
	#if it contains .00 remove it then clean it
	
	if typ == "price":
		#this will help remove unicode characters from the price
		for currency in currencies:
			if currency in data:
				data.replace(currency, "")
		data = re.sub("\.00", "", data)
		cleaned_data = re.sub("[^0-9]", "", data)
		return cleaned_data.strip()
	
	#remove \n from name
	if "\n" in data:
		data = data.replace("\n", "")

	cleaned_data = re.sub("[^0-9a-zA-Z]", " ", data)

	return cleaned_data.strip()

	#regex to remove not letters or numbers from the text

def build_search_url(base, search_term):
	return base.replace("{{query}}", urllib.quote_plus(search_term))

#this will turn relative path to absolute path
def fix_url(href, base_url):
	return urljoin(base_url, href)
	pass

#function to check if link has bad words
def has_no_bad_words(data):
	for word in forbiden_words:
		if word in data:
			return False
	return True

def passes_product_page_forward_slash_rule(data):
	if "http" in data:
		if data.count("/") > 3:
			return False
		return True

	if "https" in data:
		if data.count("/") > 3:
			return False
		return True

	if data.count("/") > 1:
		return False

	return True



#this will filter links from list of links
#the filter uses two rules.
#1. links with words from the bad words will be removed
#links that are not product pages will also be removed
#except for websites that do not abide by the rule of no more than 3 /
def filter_links(links, title, source=""):
	#filter links if none of the product title word not in link
	likely_pages = []
	words_in_title = title.split()
	for link in links:
		title_size =len(words_in_title)
		count = 0
		for word in words_in_title:
			if word.lower() in link.lower():
				count +=1
				

		diff = count/float(title_size)
		if diff >= 0.6:
			likely_pages.append(link)

	links = likely_pages

	# print "filtered links"
	# print links

	# if len(links) > 10:
	# 	links = links[0:20]


	#first filter links that have forbiden words
	filtered_links_without_bad_words = filter(has_no_bad_words, links)


	#this uses the sqllite3 db to check if a link is not in it
	learner = LinksLearner()
	filtered_links_without_non_known_non_product_pages = [x for x in filtered_links_without_bad_words if not learner.is_not_a_product_page(x)]
	#close the db connection
	learner.close()

	



	#check if source is included in exceptions and if yes return the filtered links without bad words
	if source != "":
		for exception in forward_slash_exceptions:
			if exception in source:
				return filtered_links_without_non_known_non_product_pages

	filtered_links_without_more_than_enough_forward_slash = filter(passes_product_page_forward_slash_rule, filtered_links_without_non_known_non_product_pages)

	return filtered_links_without_more_than_enough_forward_slash
	


def load_dynamic_website_and_get_links(url):
	dryscrape.start_xvfb()
	# set up a web scraping session
	sess = dryscrape.Session()

	# we don't need images
	sess.set_attribute('auto_load_images', False)
	# sess.set_attribute('automatic_reload', False)

	try:
		sess.visit(url)
	except:
		return []
	print(sess.body())

	links = []
	page_links = sess.xpath('//a[@href]')
	for link in page_links:
		try:
			href = link.get('href')
			# print href
			#do not allow irrelevant pages like contact us page to be returned
			if href:
				if href != "":
					if href not in links:
						#turn relative hrefs to absolute hrefs
						absolute_href = fix_url(href, url)
						links.append(absolute_href)
		except:
			pass
				
			

	return links

def load_dynamic_website(url):
	dryscrape.start_xvfb()
	# set up a web scraping session
	sess = dryscrape.Session()

	# we don't need images
	sess.set_attribute('auto_load_images', False)

	try:
		sess.visit(url)
	except:
		return (False, "")

	return (True, sess.body())



def load_website(url):
	req = urllib.request.Request(url, headers=hdr)
	# req = urllib2.Request(url, headers=hdr)

	try:
		response = urllib.request.urlopen(req)
	except Exception as e:
		print(e)
		return (False, "")
	return (True, response.read())


def load_website_and_get_links(url):
	links = []
	req = urllib.request.Request(url, headers=hdr)

	try:
		response = urllib.request.urlopen(req)
	except:
		return links

	body = response.read()

	doc = BeautifulSoup(body, "lxml")



	for link in doc.findAll('a'):
		href = link.get('href')
		if href:
			if href.lower() != "":
				if href not in links:
					links.append(fix_url(href, url))

	return links


	
if __name__ == "__main__":
	print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

	# print filter_links(["http://facebook.com", "http://jamb.com", "http://konga.com/iphone.html", "www.iphone.com/catala", "www.iphone.com/catala/apple", "http://twitter.com", "http://konga.com", "http://konga.com/catalog/electronics"])
	# print load_website_and_get_links("http://konga.com")

	print(build_search_url("http://www.konga.com/catalogsearch/result/?cat=0&q={{query}}", "iphone 5s black"))
	# links = load_dynamic_website_and_get_links("http://konga.com")
	#make sure to use set to prevent visiting a page twice
	# print set(links)
	# print fix_url("http://bestprices.ng/stores", "http://bestprices.ng")
	# print links
