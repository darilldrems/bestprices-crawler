#similarity ranking

def __letter_pairs(word):
	num_pairs = len(word) - 1
	pairs = []
	for index in range(num_pairs):
		pair = word[index] + word[index+1]
		pairs.append(pair)

	return pairs

def __word_letter_pairs(words):
	all_words = words.split()
	all_pairs = []
	for word in  all_words:
		all_pairs.extend(__letter_pairs(word))

	return all_pairs

def __strikeamatch_similarty_score(string1, string2):
	pairs1 = set(__word_letter_pairs(string1))
	pairs2 = set(__word_letter_pairs(string2))

	union_size = len(pairs1.union(pairs2))
	
	intersection_size = len(pairs1.intersection(pairs2))

	return intersection_size/float(union_size)
	
#this test for how many words in string1 exist in string2 and then divides by total number of words in string1	
def __offset_score(string1, string2):
	words1 = string1.split()
	
	size = len(words1)

	count = 0
	for word in words1:
		if word in string2:
			count += 1

	if count == 0:
		return 0
	else:
		return count/float(size)


def similarity_score(string1, string2):
	string1 = string1.lower()
	string2 = string2.lower()

	offset_score = __offset_score(string1, string2)
	s_score = __strikeamatch_similarty_score(string1, string2)

	# print("offsetscore"+str(offset_score))
	# print("sscore"+str(s_score))

	if offset_score == 0:
		return s_score

	if offset_score == 1.0:
		return offset_score

	if offset_score < s_score:
		return s_score

	return (s_score + offset_score)/2.0

if __name__ == "__main__":
	
	print(similarity_score("Newclime elegance ceiling fan", "Newclime ceiling fan"))

	#handle exceptions and then edit the backoffice to include is dynamic website field

