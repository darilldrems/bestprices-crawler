#service use dryscrape py to load pages
# import urllib2
import urllib
import urllib.request
import json

from settings import settings


class Service():
	def __init__(self, country):
		self.country = country

	def get_stores(self):
		response = urllib.request.urlopen(settings["endpoints"][self.country]["get_stores_url"])
		return json.loads(response.read())
		pass

	def get_products(self):
		response = urllib.request.urlopen(settings["endpoints"][self.country]["get_products_url"])
		return json.loads(response.read())
		pass

	def add_offer(self, **kwargs):
		data = {
			"product": kwargs.get('product'),
			"store": kwargs.get('store'),
			"link": kwargs.get('link'),
			"price": kwargs.get('price'),
			"pass": "golang-crawler"
		}
		encoded_data = urllib.urlencode(data)

		try:
			urllib.request.urlopen(settings["endpoints"][self.country]["add_offer_url"]+"?"+encoded_data)
		except:
			pass
		pass

	def get_old_products(self):
		response = urllib.request.urlopen(settings["endpoints"][self.country]["get_old_products_url"])
		return json.loads(response.read())
		pass

	def update_product_offers(self, offers):
		pass

if __name__ == "__main__":
	serv = Service("Nigeria")
	# print(serv.get_products())
	serv.add_offer(product="55d0aaec212616885c03b5c6", store="5536717bfd9104a31c2cc504", price="10010", link="http://www.kara.com.ng/nexus-breeze-20-industrial-chrome-fan")
