#this is 
import sqlite3

class LinksLearner():
	def __init__(self):
		self.connection = sqlite3.connect("not_product_pages.db")
		self.cursor = self.connection.cursor()
		self.cursor.execute("CREATE TABLE IF NOT EXISTS links(link varchar(255))")
		self.connection.commit()

	def is_not_a_product_page(self, link):
		obj = self.cursor.execute("SELECT * FROM links WHERE link = '%s'" % link)
		if obj.fetchone():
			return True
		return False

	#this will be used to add pages that have been discovered not to be product pages
	def add_page(self, link):
		exists = self.cursor.execute("SELECT * FROM links WHERE link = '%s'" % link)
		
		if not exists.fetchone():
			self.cursor.execute("INSERT INTO links VALUES('%s')" % link)
			self.connection.commit()
	def close(self):
		self.connection.close()
		pass

if __name__ == "__main__":
	l_lerner = LinksLearner()

	l_lerner.add_page("http://jamb.com")
	# l_lerner.add_page("http://facebook.com")

	print(l_lerner.is_not_a_product_page("http://facebook.com"))
