#product parser
#this will contain custom algorith to parse pages

import microdata
import urllib
import json

from utilities import sanitize_data, load_dynamic_website
from bs4 import BeautifulSoup
from unidecode import unidecode

from utilities import load_website


#this will use the schema.org/Product schema to fetch product name and price from product pages
def __parse_page_with_microdata(page):
	items = microdata.get_items(page)

	item = None

	product_name = ""
	product_price = ""

	if len(items) > 0:
		item = json.loads(items[0].json())
		print(items[0].json())
		if item.get("properties"):
			properties = item.get("properties")

			if properties.get("name") and len(properties.get("name")) > 0:
				product_name = properties.get("name")[0]
				product_name = sanitize_data(product_name)

			if properties.get("offers"):
				offers = properties.get("offers")[0]
				if offers.get("properties"):
					offers_properties = offers.get("properties")
					if offers_properties.get("price") and len(offers_properties.get("price")) > 0:
						product_price = offers_properties.get("price")[0]
						product_price = sanitize_data(product_price, typ="price")

			# if product_name != "" and product_price != "":
			return (True, product_name, product_price)


	return (False, "", "")

#here is a quick fix for the jumia data format problem I noticed
def get_price_quick_fix(page):
	doc = BeautifulSoup(page, "lxml")

	price_nodes = doc.find_all(attrs={"class": "price"})

	if len(price_nodes) > 0:
		raw_price = price_nodes[0].getText()
		sanitized_price = sanitize_data(raw_price, typ="price") 
		return sanitized_price

	price_nodes = doc.find_all(attrs={"class": "price-box"})
	if len(price_nodes) > 0:
		raw_price = price_nodes[0].getText()
		sanitized_price = sanitize_data(raw_price, typ="price") 
		return sanitized_price

	price_nodes = doc.find_all(attrs={"class": "prices"})
	if len(price_nodes) > 0:
		raw_price = price_nodes[0].getText()
		sanitized_price = sanitize_data(raw_price, typ="price") 
		return sanitized_price



	return ""
	# print(price_nodes)
	pass

#this will use the class names .product-name and .price to fetch parse html page for product information
def __parse_page_with_class(page):
	doc = BeautifulSoup(page, "lxml")

	# print (doc.prettify())

	product_name = ""
	product_price = ""

	name_node = doc.find(attrs={"class": "product-name"})

	if not name_node:
		name_node = doc.find(attrs={"class": "title"})

	if name_node:
		product_name = name_node.getText()



	price_node = doc.find(attrs={"class": "price-box"})

	if price_node:
		product_price = price_node.getText()
		# print "initial price: %s" % product_price

	if not price_node:
		price_node = doc.find(attrs={"class": "prices"})
		if price_node:
			print("prices: "+price_node.getText())
			prices = price_node.getText().split(" ")
			if len(prices) > 0:
				product_price = prices[0]
				# if int(sanitize_data(prices[0], typ="price")) > int(sanitize_data(prices[1], typ="price")):
					# product_price = prices[1]
			# else:
				# product_price = prices[0]
	if product_price == "":
		price_node = doc.find(attrs={"class": "price"})
		if price_node:
			product_price = price_node.getText()

	if product_price == "":
		price_node = doc.find(attrs={"class": "price-current"})
		if price_node:
			product_price = price_node.getText()

	if product_price == "":
		price_node = doc.find(attrs={"class": "regular-price"})
		if price_node:
			product_price = price_node.getText()
	# print name_node.text()

	if product_price != "" and product_name != "":
		return (True, sanitize_data(product_name), sanitize_data(product_price, typ="price"))

	return (False, product_name, product_price)

def parse_page_and_get_product(data):
	status, name, price = __parse_page_with_microdata(data)

	if not status:
		status, name, price = __parse_page_with_class(data)

	if status:
		if name != "" and price == "":
			price = get_price_quick_fix(data)
			print("ran the quick fix from jumia")
		return (True, name, price)

	return (False, name, price)

	pass

if __name__ == "__main__":
	status, response = load_website("http://www.kaymu.com.ng/luvena-fortuna-headband-socks-set-800868.html")

	# data = __parse_page_with_microdata(response)

	if status:
		data = parse_page_and_get_product(response)

		print(data)

	print("Good bye")
