#app
import sys

from service import Service
from utilities import build_search_url, load_dynamic_website_and_get_links, \
 load_website_and_get_links, filter_links, load_dynamic_website, load_website

from pageparser import parse_page_and_get_product
from similarityranking import similarity_score
from learner import LinksLearner

minimum_score = 0.5

def start(country):
	service = Service(country)

	stores_resp = service.get_stores()
	products_resp = service.get_products()

	if not stores_resp.get("stores"):
		return

	stores = stores_resp.get("stores")

	if not products_resp.get("products"):
		return

	products = products_resp.get("products")


	if len(products) == 0:
		return

	if len(stores) == 0:
		return


	for product_todo in products:
		print(product_todo.get("title"))
		for store_todo in stores:
			if store_todo.get("is_dynamic"):
				search_url = build_search_url(store_todo.get("search_path"), product_todo.get("title"))
				print("search url : "+search_url)
				if store_todo.get("is_dynamic") == "yes":
					search_result_links = load_website_and_get_links(search_url)

					if len(search_result_links) > 0:
						filtered_links = filter_links(search_result_links, product_todo.get("title"), search_url)
						
						for link in filtered_links:
							status, content = load_website(link)
							if status:
								is_product_page, product_name, product_price = parse_page_and_get_product(content)
								if is_product_page:
									score = similarity_score(product_todo.get("title"), product_name)
									if score >= minimum_score:
										service.add_offer(product=product_todo.get("_id"), store=store_todo.get("_id"), price=product_price, link=link)
								else:
									learner = LinksLearner()
									learner.add_page(link)
									learner.close()




				else:
					search_result_links = load_website_and_get_links(search_url)

					if len(search_result_links) > 0:
						filtered_links = filter_links(search_result_links, product_todo.get("title"), search_url)
						# print filtered_links
						# exit()
						# print filtered_links
						# continue
						for link in filtered_links:
							status, content = load_website(link)
							if status:
								is_product_page, product_name, product_price = parse_page_and_get_product(content)
								if is_product_page:
									score = similarity_score(product_todo.get("title"), product_name)
									if score >= minimum_score:
										service.add_offer(product=product_todo.get("_id"), store=store_todo.get("_id"), price=product_price, link=link)

								else:
									learner = LinksLearner()
									learner.add_page(link)
									learner.close()





	pass


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print("ERROR: No country passed")
	else:
		country = sys.argv[1]
		start(country)